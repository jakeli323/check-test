package com.jakeli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

public class MainTest {
	public List<File> allList = new ArrayList<>();
	public List<File> repeatList = new ArrayList<>();

	public static void main(String[] params) throws FileNotFoundException, IOException {
		MainTest main = new MainTest();
		main.process();
	}
	
	public void process() throws FileNotFoundException, IOException {
		File file = new File("C:\\Users\\lenovo\\Desktop\\final_submit _check");

		initFileList(file);
		
		for(File f : allList) {
			long modTime = f.lastModified();
			for(File f2 : allList) {
				if(f2.lastModified() == modTime && (!f.getPath().equals(f2.getPath())) && (!isContained(f))) {
					String md5a = DigestUtils.md5Hex(IOUtils.toByteArray(new FileInputStream(f)));
					String md5b = DigestUtils.md5Hex(IOUtils.toByteArray(new FileInputStream(f2)));
					if(md5a.equals(md5b)) {
						repeatList.add(f);
						System.out.println("发现重复A = " + f.getPath());
						System.out.println("发现重复B = " + f2.getPath());
					}
				}
			}
		}	
	}
	
	private boolean isContained(File f) {
		for(File file :repeatList) {
			if(f.getPath().equals(file.getPath())) {
				return true;
			}
		}
		
		return false;
	}
	
	private void initFileList(File file){
		File[] files = file.listFiles();
		
		if(files != null) {
			for(File f : files) {
				if(!f.isDirectory()) allList.add(f);
				
				initFileList(f);			
			}	
		}

	
	}
}
